# Help?
This project serves as a placeholder and entry point for reporting issues. Since
DalmatinerDB consists of many projects it can sometimes be hard to decide
where to open a ticket.

If in doubt, this is a good place!

When not in doubt there are the related sub projects most notably:

* [DalmatinerDB](https://gitlab.com/Project-FiFo/DalmatinerDB/dalmatinerdb/issues/new) - the backend store
* [DalmatinerFE](https://gitlab.com/Project-FiFo/DalmatinerDB/dalmatinerfe/issues/new) - the frontend
* [DalmatinerPX](https://gitlab.com/Project-FiFo/DalmatinerDB/dalmatinerpx/issues/new) - they protocol proxy
* [Grafana Plugin](https://gitlab.com/Project-FiFo/DalmatinerDB/dalmatinerdb-datasource/issues/new) - the Grafana plugin
